#include "utilities.h"
#include "player.h"
#include "enemy.h"
// TO DO: 1. Sort out delayScroll & associates. 2. Make final boss. 3. Improve!
using namespace std;
// Some standard strings that will be used during development.
string hold = "Placeholder text";
string err = "This is an error message. If you are seeing it, report it to the developer.";
// These are the constructors for the functions. They are not important.
void delayScroll(int delayMS, int lineCount);
character *generateEnemy();
void encounterEnemy(character *player, character *enemy);
void fightDecisions(character *player);
int eR = 0;

void delayScroll(int delayMS, int lineCount)
// Responsible for scrolling the text down (like 'clear' in UNIX)
{
	for (int i = 0; i < lineCount; i++)
	{
		cout << "" << endl;
		this_thread::sleep_for(chrono::milliseconds(delayMS));
	}
}

character *generateEnemy()
// Creates a new enemy using the enemy class (and subsequently, the character class)
{
	// Arrary for enemies (enemy database)
	string eD[5];
	// Array for bios (bio database)
	string bD[5];
	// Array for stats (stat database)
	int sD[5];
	eD[0] = "Blob";
	eD[1] = "Sentient Sword";
	eD[2] = "Rock";
	eD[3] = "Computer";
	eD[4] = "GIANT METEOR";
	bD[0] = "A strange amorphous blob accosts you, with the futile notion that it can defeat you.";
	bD[1] = "A sword, that for plot purposes, is now sentient, runs at you head first.";
	bD[2] = "Rocks really hate humans, apparently, and they chose now to revolt. It's a coincidence, I swear.";
	bD[3] = "Well, wouldn't ya know it? Humanity has spontaneously entered the Post-Singulatry, and now the computer you brought with you is sentient.";
	bD[4] = "OHHH MY GOD IT'S A GIANT METEOR THAT FOR SOME UNSPECIFIED REASON IS HEADED STRAIGHT FOR YOU!!!!!!";
	sD[0] = 2;
	sD[1] = 4;
	sD[2] = 5;
	sD[3] = 7;
	sD[4] = 10;
	character *e = new enemy(eD[eR], bD[eR]);
	e->setStats(sD[eR]);
	eR++;
	return e;
}
void encounterEnemy(character *player, character *enemy)
// Responsible for deciding whether or not the player and enemy can duke it out
{
		while (player->isAlive())
		{
			player->fight(enemy);
			if (player->isAlive() && enemy->id == "GIANT METEOR")
			{
				if (!enemy->isAlive())
				{
					return;
				}
			}
			if (enemy->isAlive())
			{
				enemy->fight(player);
			}
			else
			{
				break;
			}

		}
		fightDecisions(player);
	
}

void fightDecisions(character * player)
{
	if (player->isAlive())
	{
        character *anEnemy = generateEnemy();
        int selectedOption = 0;
        cout << anEnemy->id << " approaches." << endl;
        cout << "1. Engage" << endl;
        cout << "2. Flee" << endl;
        cout << "> ";
        cin >> selectedOption;
        if (selectedOption == 1)
        {
            if (anEnemy->id == "Blob")
            {
                string blobart =
                "      ___________      \n"
                "     /           \\     \n"
                "    /             \\   \n"
                "   /               \\  \n"
                "  |   O     _    O  |   \n"
                "  |  __________     /   \n"
                "  \\ \\_________/    /    \n"
                "   \\______________/    ";
				cout << anEnemy->bio << endl;
				this_thread::sleep_for(chrono::milliseconds(4000));
				cout << blobart << endl;
				delayScroll(10, 2);
				cout << "You have " << player->healthNow << " health out of " << player->totalHealth << " health." << endl;
				delayScroll(10, 2);
                encounterEnemy(player, anEnemy);
            }
            else if (anEnemy->id == "Sentient Sword")
            {
                string swordart =
                "    /\\ \n"
                "   /  \\  \n"
                "  | 0 0|   \n"
                "  |    |    \n"
                "  |    |    \n "
                " |    |   \n"
                "  |    |  \n"
                "  |    |  \n"
                "  |    |   \n"
                "  |____|    \n"
                "   |  |     \n"
                "   |  |      \n"
                "   0==0     \n"
                "    )(       \n"
                "    ()        ";
				cout << anEnemy->bio << endl;
				this_thread::sleep_for(chrono::milliseconds(4000));
				cout << swordart << endl;
				delayScroll(10, 2);
				cout << "You have " << player->healthNow << " health out of " << player-> totalHealth << " health." << endl;
				delayScroll(10, 2);
                encounterEnemy(player, anEnemy);
            }
            else if (anEnemy->id == "Rock")
            {
                string rockart =
                
                "          _____          \n"
                "      __ /__    \\__     \n"
                "   _ /      \\    __\\   \n"
                "  /          \\__ /  \\  \n"
                " |   O          O    |    \n"
                " |      ______      /    \n"
                "  \\     \\____/     /   \n"
                "   \\______________/     ";
				cout << anEnemy->bio << endl;
				this_thread::sleep_for(chrono::milliseconds(4000));
				cout << rockart << endl;
				delayScroll(10, 2);
				cout << "You have " << player->healthNow << " health out of " << player-> totalHealth << " health." << endl;
				delayScroll(10, 2);
                encounterEnemy(player, anEnemy);
                
            }
            else if (anEnemy->id == "Computer")
            {
                string computerart =
                "____________________ \n"
                "| ________________ |  \n"
                "| |              | |  \n"
                "| |              | |  \n"
                "| |              | |  \n"
                "| |              | |  \n"
                "| |______________| |  \n"
                "|__________________|  \n"
                "\\  1 2 3 4 5 6 7   \\  \n"
                " \\  A S D F G H J   \\  \n"
                "  \\  Z X C V B N M   \\ \n"
                "   \\  Q W E R T Y U   \\ \n"
                "    \\__________________\\ ";
				cout << anEnemy->bio << endl;
				this_thread::sleep_for(chrono::milliseconds(5000));
				cout << computerart << endl;
				delayScroll(10, 2);
				cout << "You have " << player->healthNow << " health out of " << player-> totalHealth << " health." << endl;
				delayScroll(10, 2);
                encounterEnemy(player, anEnemy);
            }
			else if (anEnemy->id == "GIANT METEOR")
			{
				string meteorart =
					"          _____          \n"
					"      __ /__    \\__     \n"
					"   _ /      \\ __ / \\   \n"
					"  /                  \\  \n"
					" |                   |    \n"
					" |                   /    \n"
					"  \\                /   \n"
					"   \\              /     \n"
					"    \\ __     ___ /       \n"
					"    \\_/  \\__   \\         ";
				cout << anEnemy->bio << endl;
				this_thread::sleep_for(chrono::milliseconds(4500));
				cout << meteorart << endl;
				delayScroll(10, 2);
				cout << "You have " << player->healthNow << " health out of " << player-> totalHealth << " health." << endl;
				delayScroll(10, 2);
				encounterEnemy(player, anEnemy);

			}
		}
        else if (selectedOption == 2)
        {
            cout << "You run off screen, ending the game." << endl;
			this_thread::sleep_for(chrono::milliseconds(3000));
			exit(0);
        }
		else
		{
			cout << "Congrats! You found the hidden third option!" << endl;
			cout << "" << endl;
			this_thread::sleep_for(chrono::milliseconds(1000));
			cout << "It does";
			this_thread::sleep_for(chrono::milliseconds(2000));
			cout << ".";
			this_thread::sleep_for(chrono::milliseconds(2000));
			cout << ".";
			this_thread::sleep_for(chrono::milliseconds(2000));
			cout << ".";
			this_thread::sleep_for(chrono::milliseconds(2000));
			cout << " " << endl;
			cout << "Absoulutely nothing." << endl;
			this_thread::sleep_for(chrono::milliseconds(1000));
			cout << "Restarting the battle..." << endl;
			this_thread::sleep_for(chrono::milliseconds(1000));
			delayScroll(10, 30);
			eR--;
			fightDecisions(player);
		}
    }
    else
    {
		cout << "You run out of health and die." << endl;
		cout << " " << endl;
		cout << "Wipe your feet on the way out." << endl;
		cout << " " << endl;
		this_thread::sleep_for(chrono::milliseconds(4000));
		exit(0);
    }
}

int main()
{

	srand(time(NULL));
	// This is the intro.
	this_thread::sleep_for(chrono::milliseconds(3000));
	cout << "Hello human." << endl;
	this_thread::sleep_for(chrono::milliseconds(2500));
	cout << "This is a generic adventure." << endl;
	this_thread::sleep_for(chrono::milliseconds(3000));
	cout << "Nothing special about it." << endl;
	this_thread::sleep_for(chrono::milliseconds(3500));
	cout << "We also don't have the budget for a graphical user interface." << endl;
	this_thread::sleep_for(chrono::milliseconds(4000));

	char playerNameRaw[100];
	// This prompts the user to specify their name, then scrolls down before creating the player and their stats.
	cout << "So uhhh...what's your name? Limit yourself to 100 characters please, we don't have the budget for more. " << endl;
	cout << "> ";
	cin.getline(playerNameRaw,sizeof(playerNameRaw));
	string playerName(playerNameRaw);

	cout << "Alright then, " << playerName << ", let's go already." << endl;
	this_thread::sleep_for(chrono::milliseconds(3000));
	delayScroll(10, 30);


	character *randomPerson = new player(playerName, hold);
	randomPerson->setStats(5);

	fightDecisions(randomPerson);

	cout << "Wait..." << endl;;
	this_thread::sleep_for(chrono::milliseconds(1000));
	cout << "How did you do that!?!?!" << endl;
	this_thread::sleep_for(chrono::milliseconds(2000));
	cout << "You literally just destroyed a GIANT METEOR with nothing but your bare hands and a few healing potions." << endl;
	this_thread::sleep_for(chrono::milliseconds(4500));
	cout << "Not to mention everything that came before it!" << endl;
	this_thread::sleep_for(chrono::milliseconds(2500));
	cout << "I don't... I... I can't...." << endl;
	this_thread::sleep_for(chrono::milliseconds(3500));
	for (int i = 0; i < 50; i++)
	{
		cout << "CANNOT COMPUTE" << endl;
	}
	this_thread::sleep_for(chrono::milliseconds(1000));
	exit(0);
}

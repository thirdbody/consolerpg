# ConsoleRPG

ConsoleRPG is an RPG with 5 different enemies to fight. Each one is depicted via ASCII art, and both the enemy's and player's stats are randomized.

Because why not.

## Maintenance

I made this project 3-or-so years ago for a C++ intro "bootcamp" for kids. This is not a high quality project by any means.

I was sick and bored in June 2018, so I patched a few bugs (including one where the linker didn't even work) and wrote up a Makefile for it. I have no intent to change the content of the game itself besides aesthetics to preserve the mildly embarrassing spirit of 2015.

## Building

The `Makefile` is written for a Linux system. I *believe* you can compile this project on Mac in Xcode by moving all source files to a subfolder named `ConsoleRPG` and opening it up. As for Windows: please no. Just no.

That being said: the `Makefile` lets you `make`, `make install` (why you would want this in your `/usr/bin` is beyond me but hey it was in my template), `make run` (`make` but it also executes the binary), and `make clean` (remove object and binary files).
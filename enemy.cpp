#include "enemy.h"

using namespace std;

void scrollText(int delay, int lineCount)
// Responsible for scrolling the text down (like 'clear' in UNIX)
{
	for (int i = 0; i < lineCount; i++)
	{
		cout << "" << endl;
		this_thread::sleep_for(chrono::milliseconds(delay));
	}
}

void enemy::fight(character *other)
{
	other->healthNow -= damage;
	scrollText(10, 2);
	cout << id << " attacked you for " << damage << " damage." << endl;
	cout << other->id << " has " << other->healthNow << " health out of " << other->totalHealth << " health." << endl;
	scrollText(10, 2);
}
void enemy::setStats(int level)
{
	int baseStat = 2 * level;
	int x = rand() % baseStat + 1;

	damage = (int)(x / 2);
	if (damage < 1)
		damage = 1;
	healthNow = totalHealth = x * 2;
}
bool enemy::isAlive()
{
	if (healthNow > 0)
	{
		return true;
	}
	else {
		cout << "\n\nYour opponent has fallen!\n" << endl;
		this_thread::sleep_for(chrono::milliseconds(4500));
		scrollText(10, 30);
		return false;
		
		
	}
}

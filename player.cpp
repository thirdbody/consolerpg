#include "player.h"

using namespace std;
// Everything related to battling (on the player's side, that is)

void textScroll(int timeBeforeScroll, int linesToScroll)
// Responsible for scrolling the text down (like 'clear' in UNIX)
{
	for (int i = 0; i < linesToScroll; i++)
	{
		cout << "" << endl;
		this_thread::sleep_for(chrono::milliseconds(timeBeforeScroll));
	}
}

void player::fight(character *other)
{
	int selectedOption = 0;
	cout << "Type '1' to fight." << endl;
	cout << "Type '2' to access your inventory." << endl;
	cout << "> ";
	cin >> selectedOption;
	if (selectedOption == 1)
	{
		other->healthNow -= damage;
		cout << "" << endl;
		cout << id << " hit " << other->id << " for " << damage << " damage." << endl;
		cout << other->id << " has " << other->healthNow << " health out of " << other->totalHealth << " health." << endl;
	}
	else if (selectedOption == 2)
	{
		int invenOption = 0;
		cout << "Type '1' to heal. Current Potion Count: " << potion << endl;
		cout << "> ";
		cin >> invenOption;
		if (invenOption == 1)
		{
			if (healthNow < totalHealth)
			{
				if (potion > 0)
				{
					healthNow += 10;
					if (healthNow >= totalHealth)
					{
						healthNow = totalHealth;
					}
					potion--;
				}
				else
				{
					textScroll(10, 2);
					cout << "You don't have any potions!" << endl;
					textScroll(10, 2);
				}
			}
			else
			{
				textScroll(10, 2);
				cout << "You don't need healing, you're at full health!" << endl;
				textScroll(10, 2);
			}
		}
	}
}
void player::setStats(int level)
{
	int baseStat = 2 * level;
	// This will be responsible for randomizing the health and damage stats
	int x = rand() % baseStat + 4;
	damage = x;
	healthNow = totalHealth = x * 3;
}
bool player::isAlive()
{
	if (healthNow > 0)
	{
		return true;
	}
	else {
		return false;
	}
}

#pragma once
#include "utilities.h"

using namespace std;

// Servers as the starting point for the player and enemies
class character {
public:
	// The character's name
	string id;
	// The character's description
	string bio;
	// Damage character deals
	int damage;
	// Total amount of health character has
	int totalHealth;
	// Health of character at present moment
	int healthNow;

	// Ignore this
	character();
	// Defines the character (as in, gives it a name & bio)
	character(string inputedName, string inputedBio);
	void introduce();
	virtual void fight(character *other) =0;
	virtual bool isAlive() =0;
	virtual void setStats(int level) =0;
};